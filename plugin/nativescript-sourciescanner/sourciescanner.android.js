"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var app = require("tns-core-modules/application");
var Observable_1 = require("rxjs/Observable");
var SourcieScanner = /** @class */ (function () {
    function SourcieScanner() {
    }
    SourcieScanner.prototype.startScanner = function () {
        return Observable_1.Observable.create(function (observer) {
            var onScanResult = function (data) {
                console.log(">>>>>>>>>>>>>>>> activity result: @ " + data.resultCode);
                if (data.requestCode === 1001 && data.resultCode == android.app.Activity.RESULT_OK) {
                    var json = data.intent.getStringExtra("products");
                    console.log(">>>>>>>>>>>>>>>>>>>>>>result recived is " + json);
                    observer.next(json);
                    observer.complete();
                    app.android.off('activityResult', onScanResult);
                }
                else {
                    observer.error(new Error("Error while receiving the product json"));
                }
            };
            app.android.on('activityResult', onScanResult);
            app.android.foregroundActivity.startActivityForResult(new android.content.Intent(app.android.context, com.sourcie.scanner.app.product.view.ProductsActivity.class), 1001);
        });
        //app.android.foregroundActivity.startActivity()
    };
    return SourcieScanner;
}());
exports.SourcieScanner = SourcieScanner;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic291cmNpZXNjYW5uZXIuYW5kcm9pZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNvdXJjaWVzY2FubmVyLmFuZHJvaWQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxrREFBb0Q7QUFDcEQsOENBQTZDO0FBSzdDO0lBQUE7SUF1QkEsQ0FBQztJQXJCVSxxQ0FBWSxHQUFuQjtRQUNJLE1BQU0sQ0FBQyx1QkFBVSxDQUFDLE1BQU0sQ0FBQyxVQUFBLFFBQVE7WUFDN0IsSUFBTSxZQUFZLEdBQUcsVUFBQyxJQUFJO2dCQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLHNDQUFzQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDdEUsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO29CQUNqRixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQTtvQkFDakQsT0FBTyxDQUFDLEdBQUcsQ0FBQywwQ0FBMEMsR0FBRyxJQUFJLENBQUMsQ0FBQTtvQkFDOUQsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUNwQixHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxZQUFZLENBQUMsQ0FBQztnQkFDcEQsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLHdDQUF3QyxDQUFDLENBQUMsQ0FBQTtnQkFDdkUsQ0FBQztZQUNILENBQUMsQ0FBQztZQUNKLEdBQUcsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLGdCQUFnQixFQUFFLFlBQVksQ0FBQyxDQUFDO1lBQy9DLEdBQUcsQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsc0JBQXNCLENBQUMsSUFDdEQsT0FBTyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDcEgsQ0FBQyxDQUFDLENBQUM7UUFFSCxnREFBZ0Q7SUFDcEQsQ0FBQztJQUNMLHFCQUFDO0FBQUQsQ0FBQyxBQXZCRCxJQXVCQztBQXZCWSx3Q0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIGFwcCBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9hcHBsaWNhdGlvblwiO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSBcInJ4anMvT2JzZXJ2YWJsZVwiO1xyXG5cclxuZGVjbGFyZSBjb25zdCBhbmRyb2lkLCBjb206IGFueVxyXG5kZWNsYXJlIGNvbnN0IEludGVudFxyXG5cclxuZXhwb3J0IGNsYXNzIFNvdXJjaWVTY2FubmVyIHtcclxuXHJcbiAgICBwdWJsaWMgc3RhcnRTY2FubmVyKCkgOiBPYnNlcnZhYmxlPFN0cmluZz4ge1xyXG4gICAgICAgIHJldHVybiBPYnNlcnZhYmxlLmNyZWF0ZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IG9uU2NhblJlc3VsdCA9IChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIj4+Pj4+Pj4+Pj4+Pj4+Pj4gYWN0aXZpdHkgcmVzdWx0OiBAIFwiICsgZGF0YS5yZXN1bHRDb2RlKTtcclxuICAgICAgICAgICAgICAgIGlmIChkYXRhLnJlcXVlc3RDb2RlID09PSAxMDAxICYmIGRhdGEucmVzdWx0Q29kZSA9PSBhbmRyb2lkLmFwcC5BY3Rpdml0eS5SRVNVTFRfT0spIHtcclxuICAgICAgICAgICAgICAgICAgICB2YXIganNvbiA9IGRhdGEuaW50ZW50LmdldFN0cmluZ0V4dHJhKFwicHJvZHVjdHNcIilcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj5yZXN1bHQgcmVjaXZlZCBpcyBcIiArIGpzb24pXHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChqc29uKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGFwcC5hbmRyb2lkLm9mZignYWN0aXZpdHlSZXN1bHQnLCBvblNjYW5SZXN1bHQpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihuZXcgRXJyb3IoXCJFcnJvciB3aGlsZSByZWNlaXZpbmcgdGhlIHByb2R1Y3QganNvblwiKSlcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICBhcHAuYW5kcm9pZC5vbignYWN0aXZpdHlSZXN1bHQnLCBvblNjYW5SZXN1bHQpO1xyXG4gICAgICAgICAgICBhcHAuYW5kcm9pZC5mb3JlZ3JvdW5kQWN0aXZpdHkuc3RhcnRBY3Rpdml0eUZvclJlc3VsdChuZXdcclxuICAgICAgICAgICAgYW5kcm9pZC5jb250ZW50LkludGVudChhcHAuYW5kcm9pZC5jb250ZXh0LCBjb20uc291cmNpZS5zY2FubmVyLmFwcC5wcm9kdWN0LnZpZXcuUHJvZHVjdHNBY3Rpdml0eS5jbGFzcyksIDEwMDEpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIFxyXG4gICAgICAgIC8vYXBwLmFuZHJvaWQuZm9yZWdyb3VuZEFjdGl2aXR5LnN0YXJ0QWN0aXZpdHkoKVxyXG4gICAgfVxyXG59XHJcbiJdfQ==