import * as app from "tns-core-modules/application";
import { Observable } from "rxjs/Observable";

declare const android, com: any
declare const Intent

export class SourcieScanner {

    public startScanner() : Observable<String> {
        return Observable.create(observer => {
            const onScanResult = (data) => {
                console.log(">>>>>>>>>>>>>>>> activity result: @ " + data.resultCode);
                if (data.requestCode === 1001 && data.resultCode == android.app.Activity.RESULT_OK) {
                    var json = data.intent.getStringExtra("products")
                    console.log(">>>>>>>>>>>>>>>>>>>>>>result recived is " + json)
                    observer.next(json);
                    observer.complete();
                    app.android.off('activityResult', onScanResult);
                } else {
                    observer.error(new Error("Error while receiving the product json"))
                }
              };
            app.android.on('activityResult', onScanResult);
            app.android.foregroundActivity.startActivityForResult(new
            android.content.Intent(app.android.context, com.sourcie.scanner.app.product.view.ProductsActivity.class), 1001);
        });
        
        //app.android.foregroundActivity.startActivity()
    }
}
