const CarouselItem = require("nativescript-carousel").CarouselItem;
const Carousel = require("nativescript-carousel").Carousel;
import { isAndroid } from "platform";
import { View } from "ui/core/view";

function carouselItemFromView(view: View) {
    const item = new CarouselItem();
    item.addChild(view);

    return item;
}

function addItemToCarousel(carousel: typeof Carousel) {
    return (item: typeof CarouselItem, index: number) => {
        carousel.addChild(item);
        if (isAndroid) {
            const adapter = carousel.android.getAdapter();
            if (adapter) {
                const count = index + 1;
                adapter.notifyDataSetChanged();
                carousel._pageIndicatorView.setCount(count);

                if (count === 1) {
                    carousel._pageIndicatorView.setSelection(item.android);
                }
            }
        }
    };
}

export function renderCarouselSlides(
    carousel: typeof Carousel,
    items: Array<any>,
    slideRenderer: (item: any) => View
): void {
    items
        .map(slideRenderer)
        .map(carouselItemFromView)
        .map(addItemToCarousel(carousel));
    carousel.refresh();
}
