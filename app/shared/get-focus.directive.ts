import { Directive, ElementRef } from "@angular/core";

@Directive({
  selector: "[appGetFocus]"
})
export class GetFocusDirective {

  constructor(el: ElementRef) {
    setTimeout(() => {
      if (!el.nativeElement.text) {
         el.nativeElement.focus();
      }
    }, 0);
  }
}
