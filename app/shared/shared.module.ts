import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
// Declare Module Here
import { NgShadowModule } from "../thirdparty/nativescript-ng-shadow/src/angular/ng-shadow.module";

// Declare Pipe Here
import { ActionBarComponent } from "./action-bar/action-bar.component";
import { FilterBySelected } from "./filterbyselected-pipe";
import { GetFocusDirective } from "./get-focus.directive";
import { GroupByPipe } from "./groupBy-pipe";
import { ListPickerComponent } from "./list-picker/list-picker.component";

@NgModule({
    imports: [
        NativeScriptModule,
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        NativeScriptHttpClientModule,
        NativeScriptUIListViewModule,
        NgShadowModule
    ],
    exports: [
        NativeScriptModule,
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        NativeScriptHttpClientModule,
        NativeScriptUIListViewModule,
        NgShadowModule,
        GetFocusDirective,
        GroupByPipe,
        FilterBySelected,
        ActionBarComponent
    ],
    declarations: [
        ListPickerComponent,
        GetFocusDirective,
        GroupByPipe,
        FilterBySelected,
        ActionBarComponent
    ],

    entryComponents: [
        ListPickerComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SharedModule { }
