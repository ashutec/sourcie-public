import { Injectable } from "@angular/core";
import { fromAsset, fromBase64, fromData, fromFile,
    fromFileOrResource, ImageSource } from "image-source";
import { Observable } from "rxjs/Observable";
import * as fs from "tns-core-modules/file-system";
import { Image } from "tns-core-modules/ui/image/image";

@Injectable()
export class ImageService {
    private images = new Array<any>();
    getListOfSaveImage() {
        this.images = [];
        const folder = fs.knownFolders.documents();
        console.log("Folder Get Image:" + JSON.stringify(folder));

        folder.getEntities().then((entities) => {
            entities.forEach((entity: any) => {
                // console.log(JSON.stringify(entity));
                if (entity._extension === ".png") {
                    console.log("File Name :" + entity.name);
                    const path = fs.path.join(folder.path, entity.name);
                    const data = { image: path };
                    this.images.unshift(data);
                    console.log("ImageService :" + JSON.stringify(this.images));
                }
            });
        }) // .then(() => { return this.images })
            .catch((error) =>
                console.log("Error in Save image :" + error));
    }
}
