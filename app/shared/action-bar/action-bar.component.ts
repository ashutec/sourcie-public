import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
    selector: "ns-action-bar",
    moduleId: module.id,
    templateUrl: "action-bar.component.html",
    styleUrls: ["action-bar.component.css"]
})

export class ActionBarComponent {

    @Input() showSave;
    @Input() showSearch;
    @Input() title;
    @Output() back: EventEmitter<any> = new EventEmitter<any>();
    @Output() save: EventEmitter<any> = new EventEmitter<any>();
    @Output() search: EventEmitter<any> = new EventEmitter<any>();
}
