import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "sgFilterBySelected" })
export class FilterBySelected implements PipeTransform {
  transform(value: Array<any>, field: string): Array<any> {
    const temp = value.filter((x) => x.isSelected === true);
    if (temp) {
      return temp;
    } else {
      return [];
    }
  }
}
