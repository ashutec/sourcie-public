import { Component } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { ListPicker } from "tns-core-modules/ui/list-picker/list-picker";
import { Page } from "tns-core-modules/ui/page/page";

@Component({
    selector: "ns-list-picker",
    moduleId: module.id,
    templateUrl: "list-picker.component.html"
})

export class ListPickerComponent {
    list;
    constructor(private _modalDialogParams: ModalDialogParams, private page: Page) {
        this.page.on("unloaded", () => {
          // using the unloaded event to close the modal when there is user interaction
          // e.g. user taps outside the modal page
          this._modalDialogParams.closeCallback();
        });
        this.list = this._modalDialogParams.context.list;
    }
    onItemTap(args) {
        this._modalDialogParams.closeCallback(args.index);
    }
}
