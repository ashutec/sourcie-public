export const BASE_API_URL = "";

export const transition = {
    name: "slideLeft",
    duration: 250,
    curve: "linear"
};

export function createLoadingOption(message) {
    return {
        message,
        progress: 0.65,
        android: {
            indeterminate: true,
            cancelable: false,
            cancelListener: (dialog) => console.log("Loading cancelled"),
            max: 100,
            progressNumberFormat: "%1d/%2d",
            progressPercentFormat: 0.53,
            progressStyle: 1,
            secondaryProgress: 1
        }
    };
}

export function onTouchFloatBtnUtility(args) {
    const theBtn = args.view;
    switch (args.action) {
      case "down":
        theBtn.className = "float-btn down";
        break;
      case "up":
        theBtn.className = "float-btn";
        break;
    }
  }
