import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { registerElement } from "nativescript-angular";

import { AppRoutingModule } from "./app.routing";

// Declare Module Here
import { HomeModule } from "./home/home.module";
import { setStatusBarColors, SharedModule } from "./shared";

// Declare Component Here
import { AppComponent } from "./app.component";

setStatusBarColors();
registerElement("Carousel", () => require("nativescript-carousel").Carousel);
registerElement("CarouselItem", () => require("nativescript-carousel").CarouselItem);

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        SharedModule,
        AppRoutingModule,
        HomeModule
    ],
    declarations: [
        AppComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class AppModule { }
