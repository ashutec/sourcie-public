import { Injectable } from "@angular/core";
import * as cloneDeep from "lodash/cloneDeep";
import * as sortBy from "lodash/sortBy";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

const sqlite = require("nativescript-sqlite");

import { Supplier } from "./supplier";

@Injectable()
export class SupplierService {

    suppliers$: BehaviorSubject<Array<Supplier>>;
    supplierChanged$: BehaviorSubject<Supplier>;
    private suppliers: Array<Supplier>;
    private supplier: Supplier;
    private database: any;

    constructor() {
        this.suppliers = [];
        this.supplier = new Supplier();
        this.suppliers$ = new BehaviorSubject<Array<Supplier>>(this.suppliers);
        this.supplierChanged$ = new BehaviorSubject<Supplier>(this.supplier);
    }

    setSupplier(supplier) {
        this.supplier = supplier;
        this.supplierChanged$.next(this.supplier);
    }
    /** This all method are use for SQLite Database   */
    insert(supplier) {
        const prepareQuery = `INSERT INTO supplier (name,email,contact,
            phone,business_card,serverId,isSynced,photo) VALUES (?,?,?,?,?,?,?,?)`;
        const params = [supplier.name, supplier.email, supplier.contact,
        supplier.phone, supplier.business_card, supplier.serverId, supplier.isSynced, supplier.photo];

        return this.database
            .execSQL(prepareQuery, params)
            .then((id) => {
                supplier.id = id;
                this.suppliers.push(supplier);
                this.suppliers$.next(this.createGroup());
            })
            .catch((error) => console.log(`Supplier insert failed with error`, error));
    }

    update(supplier) {
        const prepareQuery = `UPDATE supplier SET
                                name=  ?, email=  ?, contact=  ?, phone=  ?,
                                business_card=  ?, serverId =   ?, photo =   ?
                                WHERE id =   ?`;
        const params = [supplier.name, supplier.email, supplier.contact, supplier.phone,
        supplier.business_card, supplier.serverId, supplier.photo,
        supplier.id];

        return this.database
            .execSQL(prepareQuery, params)
            .then(() => {
                const index = this.suppliers.findIndex((s) => s.id === supplier.id);
                if (index > -1) {
                    this.suppliers[index] = supplier;
                    this.suppliers$.next(this.createGroup());
                }
            })
            .catch((error) => console.log(`Supplier update failed with error`, error));
    }

    fetchDebug() {
        const prepareQuery = `select * from supplier`;
        this.database.all(prepareQuery, (err, row) => console.log("Row of data was: ", row));
    }

    delete(supplier) {
        const prepareQuery = `DELETE FROM supplier WHERE id = ?`;
        const params = [supplier.id];

        return this.database
            .execSQL(prepareQuery, params)
            .then(() => {
                const index = this.suppliers.findIndex((s) => s.id === supplier.id);
                if (index > -1) {
                    this.suppliers.splice(index, 1);
                    this.suppliers$.next(this.createGroup());
                }
            })
            .catch((error) => console.log(`Supplier delete failed with error`, error));
    }

    createTable() {
        const prepareQuery = `CREATE TABLE IF NOT EXISTS supplier
                                (id INTEGER PRIMARY KEY AUTOINCREMENT,
                                name TEXT,email TEXT,
                                contact TEXT,
                                website TEXT,
                                phone TEXT,
                                business_card TEXT,
                                serverId TEXT,
                                isSynced TEXT,
                                photo TEXT)`;

        return (new sqlite("sourcie.db"))
            .then((db) => this.database = db)
            .catch((error) => console.log(`Error opening DB: ${error}`))
            .then(() => this.database.execSQL(prepareQuery))
            .then(() => console.log(`supplier table is created`))
            .then(() => this.fetchAll())
            .then(() => this.suppliers$.next(this.createGroup()))
            .catch((error) => console.log(`Error creating supplier table:${error}`));
    }

    private fetchAll() {
        const prepareQuery = `SELECT * FROM supplier ORDER BY name ASC`;

        return this.database.all(prepareQuery)
            .then((rows) => this.parseSuppliers(rows))
            .catch((error) => console.log(`Supplier query failed with error`, error));
    }

    private parseSuppliers(rows) {
        const suppliers = rows.map((row) => {
            const supplier = {
                id: row[0],
                name: row[1],
                email: row[2],
                contact: row[3],
                website: row[4],
                phone: row[5],
                business_card: row[6],
                isSynced: row[8],
                serverId: row[7],
                photo: row[9],
                type: "item"
            };

            return supplier;
        });
        this.suppliers = cloneDeep(suppliers);
    }

    private createGroup() {
        this.suppliers = sortBy(this.suppliers, ["name"]);
        let previousSupplier = { name: "" };
        const groupList = [];
        this.suppliers.map((supplier) => {
            supplier.type = "item";
            if (supplier.name.charAt(0).toLocaleUpperCase() !== previousSupplier.name.charAt(0).toLocaleUpperCase()) {
                const group = {
                    name: supplier.name.charAt(0).toLocaleUpperCase(),
                    type: "group"
                };
                previousSupplier = supplier;
                groupList.push(group);
            }
            groupList.push(supplier);
        });

        return groupList;
    }

}
