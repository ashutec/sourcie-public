import { ModuleWithProviders } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { AddNewSupplierDetailsComponent
} from "../supplier/add-new-supplier-details/add-new-supplier-details.component";
import { AddNewSupplierComponent } from "../supplier/add-new-supplier/add-new-supplier.component";

import { BusinessCardComponent } from "../supplier/business-card/business-card.component";
import { SuppliersListComponent } from "../supplier/suppliers-list/suppliers-list.component";
import { SelectSupplierComponent } from "./select-supplier/select-supplier.component";

const routes: Routes = [

    { path: "selectSupplier", component: SelectSupplierComponent },
    { path: "addNewSupplier", component: AddNewSupplierComponent },
    { path: "suppliersList", component: SuppliersListComponent },
    { path: "businessCard", component: BusinessCardComponent },
    { path: "addNewSupplierDetails", component: AddNewSupplierDetailsComponent }
];

export const supplierRoutingModule: ModuleWithProviders = RouterModule.forChild(routes);
export const supplierNavigatableComponents = [
    SelectSupplierComponent,
    AddNewSupplierComponent,
    BusinessCardComponent,
    AddNewSupplierDetailsComponent
];
