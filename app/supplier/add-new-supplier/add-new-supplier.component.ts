import { Component, OnInit } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";

import { ProductService } from "../../product/product.service";

@Component({
    selector: "ns-add-new-supplier",
    moduleId: module.id,
    templateUrl: "add-new-supplier.component.html",
    styleUrls: ["add-new-supplier.component.css"]
})

export class AddNewSupplierComponent implements OnInit {
     supplier;
     isFocus: boolean;
     constructor(private _modalDialogParams: ModalDialogParams,
                 private _productService: ProductService) { }

     ngOnInit() {
        this.supplier = this._modalDialogParams.context.supplier ?
        { ...this._modalDialogParams.context.supplier } : {} ;
        this.isFocus = false;
        if (this.supplier.name) {
            this.isFocus = true;
        }
    }

     focus() {
        this.isFocus = true;
    }

     cancel() {
        this._modalDialogParams.closeCallback();
    }

     onSaveAndClose() {
        if (this.supplier.name === "" || this.supplier.name === undefined) {
            alert("Add supplier Name");
        } else {
            this._modalDialogParams.closeCallback(this.supplier);
        }
    }
}
