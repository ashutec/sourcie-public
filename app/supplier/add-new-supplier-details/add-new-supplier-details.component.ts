import { Component, NgZone, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { NgModel } from "@angular/forms";
import * as fs from "file-system";
import { fromAsset, fromFile, ImageSource } from "image-source";
import { RouterExtensions } from "nativescript-angular/router";
import { Task } from "nativescript-background-http";
import { requestPermissions, takePicture } from "nativescript-camera";
import { LoadingIndicator } from "nativescript-loading-indicator";
import * as connectivity from "tns-core-modules/connectivity";
import { Page } from "tns-core-modules/ui/page/page";

import { createLoadingOption } from "../../shared";
import { Supplier } from "../supplier";
import { SupplierServerService } from "../supplier-server.service";
import { SupplierService } from "../supplier.service";

@Component({
    selector: "ns-add-new-supplier-details",
    moduleId: module.id,
    templateUrl: "add-new-supplier-details.component.html",
    styleUrls: ["add-new-supplier-details.component.css"]
})

export class AddNewSupplierDetailsComponent implements OnDestroy, OnInit {

    supplier: Supplier;
    task: Task;
    loader: LoadingIndicator;
    // This is magic variable
    // it delay complex UI show Router navigation can finish first to have smooth transition
    renderView = false;

    @ViewChild("company") company: NgModel;
    @ViewChild("contactName") contactName: NgModel;
    @ViewChild("email") email: NgModel;

    constructor(
        private routerExtensions: RouterExtensions,
        private page: Page,
        private supplierService: SupplierService,
        private supplierServerService: SupplierServerService,
        private ngZone: NgZone) {

        this.loader = new LoadingIndicator();
        this.supplier = new Supplier();
    }

    ngOnInit() {
        this.supplierService.supplierChanged$
            .subscribe((supplier) => this.supplier = supplier);
        // update to variable needed to do in ngZone otherwise it did not understand it
        this.page.on("loaded", () => this.ngZone.run(() => this.renderView = true));
    }
    ngOnDestroy() {
        this.page.off("loaded");
    }
    gotoCamera() {
        this.onTakePictureTap();
    }

    onTakePictureTap() {
        requestPermissions();
        takePicture({ width: 300, height: 300, keepAspectRatio: false, saveToGallery: false })
            .then((imageAsset) => setTimeout(() => this.saveImage(imageAsset), 50))
            .catch((error) => console.log(`There is problem with camera:${error}`));
    }

    goBack() {
        this.routerExtensions.back();
    }

    onSave() {
        if (this.email.valid && this.contactName.valid && this.company.valid) {
            this.saveSupplier();
        } else {
            this.showValidationMessage();
        }
    }

    private saveImage(imageAsset) {
        const source = new ImageSource();
        source.fromAsset(imageAsset).then((imageSource) => {
            const folder = fs.knownFolders.currentApp();
            const fileName = `${Math.random().toString(36).substr(2, 10)}.png`;
            const path = fs.path.join(folder.path, fileName);
            const saved = imageSource.saveToFile(path, "png");
            if (saved) {
                this.supplier.business_card = path;
                this.supplier.imageList = [path];
            }
        });
    }

    private saveSupplier() {
        const connectionType = connectivity.getConnectionType();
        switch (connectionType) {
            case connectivity.connectionType.none:
                (this.supplier.id ? this.updateSupplierLocal(this.supplier) : this.saveSupplierOnLocal(this.supplier));
                break;
            case connectivity.connectionType.wifi:
            case connectivity.connectionType.mobile:
                this.updateSupplierOnServer();
                break;
        }
    }

    private completeSave() {
        this.loader.hide();
        this.routerExtensions.back();
    }

    private saveSupplierOnLocal(data) {
        if (data.errors) {
            alert(data.errors[0].message);
        } else {
            this.supplier.isSynced = "true";
            if (data.supplier && data.supplier.id) {
                this.supplier.serverId = data.supplier.id;
            }
            this.supplierService.insert(this.supplier)
                .then((res) => this.completeSave());
        }
    }

    private saveSupplierOnServer() {
        if (this.supplier.imageList && this.supplier.imageList[0]) {
            const options = createLoadingOption(`Supplier is uploading`);
            this.loader.show(options);
            this.task = this.supplierServerService.saveSupplierWithImage(this.supplier);
            this.task.on("error", (e) => this.logEvent(e));
            this.task.on("responded", (e) => this.logEvent(e));
            this.task.on("complete", (e) => this.logEvent(e));
        } else {
            this.supplierServerService.saveSupplierWithoutImage(this.supplier)
                .subscribe((res) => {
                    (this.supplier.id ? this.updateSupplierLocal(this.supplier)
                    : this.saveSupplierOnLocal(this.supplier));
                });
        }
    }

    private logEvent(e) {
        switch (e.eventName) {
            case "complete":
                console.log("complete");
                break;
            case "error":
                console.log("error");
                alert("Upload error" + e);
                break;
            case "responded":
                const data = JSON.parse(e.data);
                (this.supplier.id ? this.updateSupplierLocal(this.supplier) : this.saveSupplierOnLocal(this.supplier));
            default:
                break;
        }
    }

    private updateSupplierOnServer() {
        if (this.supplier.serverId) {
            this.supplierServerService.updateSupplier(this.supplier)
                .subscribe((res) => this.updateSupplierLocal(res));
        } else {
            this.saveSupplierOnServer();
        }
    }

    private updateSupplierLocal(data) {
        if (data.errors) {
            alert(data.errors[0].message);
        } else {
            if (data.supplier && data.supplier.id) {
                this.supplier.serverId = data.supplier.id;
            }
            this.supplierService.update(this.supplier)
                .then((res) => this.completeSave());
        }
    }

    private showValidationMessage() {
        this.email.control.setErrors(this.email.validator(this.email.control));
        this.email.control.markAsDirty();
        this.contactName.control.setErrors(this.contactName.validator(this.contactName.control));
        this.contactName.control.markAsDirty();
        this.company.control.setErrors(this.company.validator(this.company.control));
        this.company.control.markAsDirty();
    }
}
