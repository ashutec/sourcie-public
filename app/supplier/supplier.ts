export class Supplier {
    id: string;
    name: string;
    email: string;
    contact: string;
    phone: string;
    website: string;
    // tslint:disable-next-line:variable-name
    business_card: string;
    isSynced: string;
    photo: string;
    imageFolder: string;
    isSelectedSupplier: boolean;
    imageList: Array<string>;
    serverId?: string;
    type?: string;
}
