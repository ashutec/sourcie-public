import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { SharedModule } from "../shared";

import { supplierNavigatableComponents, supplierRoutingModule } from "./supplier.routing";

// Declare Module Here
import { AddNewSupplierDetailsComponent
} from "../supplier/add-new-supplier-details/add-new-supplier-details.component";
import { AddNewSupplierComponent } from "../supplier/add-new-supplier/add-new-supplier.component";
import { BusinessCardComponent } from "../supplier/business-card/business-card.component";
import { SupplierCardComponent } from "../supplier/supplier-list-card/supplier-list-card.component";
import { SupplierServerService } from "../supplier/supplier-server.service";
import { SupplierService } from "../supplier/supplier.service";
import { SuppliersListComponent } from "../supplier/suppliers-list/suppliers-list.component";

@NgModule({
    imports: [
        SharedModule,
        supplierRoutingModule
    ],
    exports: [
        SuppliersListComponent
    ],
    declarations: [
        supplierNavigatableComponents,
        SupplierCardComponent,
        SuppliersListComponent,
        AddNewSupplierComponent,
        BusinessCardComponent,
        AddNewSupplierDetailsComponent
    ],
    providers: [SupplierService, SupplierServerService],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SupplierModule { }
