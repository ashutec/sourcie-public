import { Component, OnDestroy, OnInit, ViewContainerRef } from "@angular/core";
import * as cloneDeep from "lodash/cloneDeep";
import { ModalDialogService } from "nativescript-angular/modal-dialog";
import { RouterExtensions } from "nativescript-angular/router";

import { Page } from "tns-core-modules/ui/page/page";

import { Product } from "../../product/product";
import { ProductService } from "../../product/product.service";
import { SupplierService } from "../supplier.service";

import { onTouchFloatBtnUtility } from "../../shared";
import { addNewSupplierUtility } from "../util";

@Component({
  selector: "ns-supplier-list",
  moduleId: module.id,
  templateUrl: "./select-supplier.component.html",
  styleUrls: ["select-supplier.component.css"]
})

export class SelectSupplierComponent implements OnDestroy, OnInit {

  suppliers = [];
  selectedSuppliers: Array<any> = [];
  isSelectedData: boolean = false;
  renderView = false;

  constructor(private routerExtensions: RouterExtensions,
              private productService: ProductService,
              private supplierService: SupplierService,
              private page: Page,
              private modalDialogService: ModalDialogService,
              private viewContainerRef: ViewContainerRef) {
  }
  ngOnInit() {
    this.supplierService.suppliers$
      .subscribe((suppliers) => this.suppliers = cloneDeep(suppliers));

    this.page.on("loaded", () => this.renderView = true);
  }

  ngOnDestroy() {
    this.page.off("loaded");
}

  templateSelector(item: any, index: number, items: any) {
    return item.type;
  }

  goBack() {
    this.routerExtensions.back();
  }

  onItemSelected(supplier) {
    this.suppliers.map((data) => {
      if (data.id === supplier.id) {
        data.isSelected = !data.isSelected || data.isSelected === "false" ? "true" : "false";
      }
    });
    this.checkSelectedSuppliers();
  }

  onTapAddSupplier() {
    if (this.selectedSuppliers[0]) {
      const data = this.selectedSuppliers[0];
      const supplier = {
        name: data.name,
        id: data.id
      };
      this.productService.setSupplier(supplier);
    }
    this.routerExtensions.back();
  }

  checkSelectedSuppliers() {
    this.selectedSuppliers = this.suppliers.filter((supplier) => supplier.isSelected === "true");
    this.isSelectedData = this.selectedSuppliers.length > 0 ? true : false;
  }
  onTouchFloatBtn(args) {
    onTouchFloatBtnUtility(args);
  }

  addNewSupplier() {
    addNewSupplierUtility(this.modalDialogService, this.supplierService, this.viewContainerRef);
  }

}
