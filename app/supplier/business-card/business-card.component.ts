import { Component } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { RouterExtensions } from "nativescript-angular/router";
import { transition } from "../../shared";
import { SupplierService } from "../supplier.service";

@Component({
    selector: "ns-business-card",
    moduleId: module.id,
    templateUrl: "business-card.component.html",
    styleUrls: ["business-card.component.css"]
})

export class BusinessCardComponent {

    constructor(
        private routerExtensions: RouterExtensions,
        private modalDialogParams: ModalDialogParams,
        private supplierService: SupplierService) { }

    // this function will add supplier in supplier list.
    skip() {
        this.modalDialogParams.closeCallback("SKIP_DETAILS");
    }
    yes() {
        const supplier = this.modalDialogParams.context.supplier ?
        this.modalDialogParams.context.supplier : {} ;
        this.supplierService.setSupplier(supplier);
        this.routerExtensions.navigate(["/addNewSupplierDetails"], { transition });
        this.modalDialogParams.closeCallback("ADD_DETAILS");
    }
}
