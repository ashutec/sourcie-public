import { Component, EventEmitter, Input, Output } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { ProductService } from "../../product/product.service";
import { transition } from "../../shared";
import { Supplier } from "../supplier";

@Component({
    selector: "ns-supplier-card",
    moduleId: module.id,
    templateUrl: "supplier-list-card.component.html",
    styleUrls: ["supplier-list-card.component.css"]
})

export class SupplierCardComponent {

    @Input() supplier;
    @Input() showAddProduct;
    @Output() tap: EventEmitter<Supplier> = new EventEmitter<Supplier>();
    addNewProduct;
    constructor(private routerExtensions: RouterExtensions,
                private _productService: ProductService) {

    }

    addNewProducts(supplier) {
        this._productService.setProduct({});
        this._productService.setSupplier(supplier);
        this.routerExtensions.navigate(["/newProduct", { source : "SUPPLIER_LIST" }], { transition });
    }
}
