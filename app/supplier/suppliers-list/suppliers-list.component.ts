import { Component, EventEmitter, OnInit, ViewContainerRef } from "@angular/core";
import { ModalDialogService } from "nativescript-angular/modal-dialog";
import { RouterExtensions } from "nativescript-angular/router";
import { ListViewEventData, RadListView } from "nativescript-ui-listview";

import { Supplier } from "../supplier";
import { SupplierService } from "../supplier.service";

import * as cloneDeep from "lodash/cloneDeep";
import { onTouchFloatBtnUtility, transition } from "../../shared";
import { addNewSupplierUtility } from "../util";

@Component({
    selector: "ns-suppliers",
    moduleId: module.id,
    templateUrl: "suppliers-list.component.html",
    styleUrls: ["suppliers-list.component.css"]
})

export class SuppliersListComponent implements OnInit {
     suppliers = [];

     constructor(private supplierService: SupplierService,
                 private routerExtensions: RouterExtensions,
                 private modalDialogService: ModalDialogService,
                 private viewContainerRef: ViewContainerRef) {
                     // initialize supplier table
                     this.supplierService.createTable();

    }
     ngOnInit() {
        this.supplierService.suppliers$.subscribe((suppliers) => {
            this.suppliers = suppliers;
        });
    }
     templateSelector(item: any, index: number, items: any) {
        return item.type;
    }

     addNewSupplier() {
        addNewSupplierUtility(this.modalDialogService, this.supplierService, this.viewContainerRef);
    }

     onTouchFloatBtn(args) {
        onTouchFloatBtnUtility(args);
      }

     onItemSelected(supplier) {
        this.supplierService.setSupplier(cloneDeep(supplier));
        this.routerExtensions.navigate(["/addNewSupplierDetails"], { transition });
    }
}
