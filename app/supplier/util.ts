import { ViewContainerRef } from "@angular/core";
import { ModalDialogOptions, ModalDialogService } from "nativescript-angular/modal-dialog";
import { AddNewSupplierComponent } from "./add-new-supplier/add-new-supplier.component";
import { BusinessCardComponent } from "./business-card/business-card.component";
import { SupplierService } from "./supplier.service";

export function addNewSupplierUtility(modalDialogService: ModalDialogService,
                                      supplierService: SupplierService,
                                      viewContainerRef: ViewContainerRef) {
    let newSupplier;
    const options: ModalDialogOptions = {
        viewContainerRef,
        fullscreen: false,
        context: { supplier : {} }
    };

    modalDialogService.showModal(AddNewSupplierComponent, options)
        .then((supplier) => {
            if (supplier && supplier.name) {
                newSupplier = supplier;
                options.context = { supplier };

                return modalDialogService.showModal(BusinessCardComponent, options);
            }})
        .then((action) => {
                    if (action === "SKIP_DETAILS") {
                        supplierService.insert(newSupplier);
                    }
                });
}
