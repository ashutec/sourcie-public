import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { session, Session } from "nativescript-background-http";

import { BASE_API_URL } from "../shared";

@Injectable()
export class SupplierServerService {
    private session: Session;

    constructor(private http: HttpClient) {
        this.session = session("image-upload");
    }

    saveSupplierWithImage(supplier) {
        const params = [
            { name: "company", value: supplier.name },
            { name: "email", value: supplier.email },
            { name: "name", value: supplier.contact },
            { name: "phone", value: supplier.phone },
            { name: "business_card", filename: supplier.imageList[0], mimeType: "image/png" }
        ];
        const url = `${BASE_API_URL}/suppliers.json`;
        const request = {
            url,
            method: "POST"
        };

        return this.session.multipartUpload(params, request);
    }

    saveSupplierWithoutImage(supplier) {
        const params = {
            company: supplier.name,
            email: supplier.email,
            name: supplier.contact,
            phone: supplier.phone
        };
        const url = `${BASE_API_URL}/suppliers.json`;

        return this.http.post(url, params);
    }

    updateSupplier(supplier) {
        const params = {
            company: supplier.name,
            email: supplier.email,
            name: supplier.contact,
            phone: supplier.phone
        };
        const url = `${BASE_API_URL}/suppliers/${supplier.serverId}.json`;

        return this.http.put(url, params);
    }
}
