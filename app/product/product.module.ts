import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { productNavigatableComponents, productRoutingModule } from "./product.routing";

// Declare Module Here
import { SharedModule } from "../shared";
import { ProductsComponent } from "./products.component";

// Declare Component Here
import { CategoryComponent } from "./category/category.component";
import { ImageCardComponent } from "./image-card/image-card.component";
import { NewProductComponent } from "./new-product/new-product.component";

// Declare Service Here
import { ProductServerService } from "./product-server.service";
import { ProductService } from "./product.service";

@NgModule({
    imports: [
        SharedModule,
        productRoutingModule
    ],
    exports: [
        ProductsComponent
    ],
    declarations: [
        productNavigatableComponents,
        ImageCardComponent
    ],
    providers: [ProductService, ProductServerService],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ProductModule { }
