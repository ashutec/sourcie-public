import { Injectable } from "@angular/core";
const sqlite = require("nativescript-sqlite");
import * as cloneDeep from "lodash/cloneDeep";
import * as sortBy from "lodash/sortBy";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

import { Product } from "./product";

@Injectable()
export class ProductService {

    products$: BehaviorSubject<Array<Product>>;
    productChanged$: BehaviorSubject<Product>;
    private products: Array<Product>;
    private product: Product;
    private database: any;

    constructor() {
        this.product = new Product();
        this.products = [];
        this.products$ = new BehaviorSubject<Array<Product>>(this.products);
        this.productChanged$ = new BehaviorSubject<Product>(this.product);
    }

    setProduct(product) {
        this.product = product;
        this.productChanged$.next(this.product);
    }

    setSupplier(supplier) {
        this.product.supplierId = supplier.id ? supplier.id : this.product.supplierId;
        this.product.supplierName = supplier.name ? supplier.name : this.product.supplierName;
        this.productChanged$.next(this.product);
    }

    setCategory(category) {
        this.product.category = category.name;
        this.productChanged$.next(this.product);
    }
    /** This all method are use for SQLite Database   */

    insert(product) {
        const prepareQuery = `INSERT INTO product
        (assignedProject,name,supplierId,supplierName,
         category,description,rating,price,currency,
         moq,privateNotes,imageFolder,imageList,
         isSynced,isSelected,serverId)
         VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;

        const params = [product.assignedProject, product.name, product.supplierId, product.supplierName,
        product.category, product.description, product.rating, product.price, product.currency,
        product.moq, product.privateNotes, product.imageFolder, product.imageList,
        product.isSynced, product.isSelected, product.serverId];

        return this.database
            .execSQL(prepareQuery, params)
            .then((id) => {
                product.id = id;
                this.products.push(product);
                this.products$.next(this.createGroup());
            })
            .catch((error) => console.log(`Product insert failed with error`, error));
    }

    update(product) {
        const prepareQuery = `UPDATE product SET assignedProject = ?,
            name= ?, supplierId =  ?, supplierName =  ?, category =  ?,
            description =  ?, rating =  ?, price =  ?, currency =  ?,
             moq =  ?, privateNotes =  ?, imageFolder =  ?,
            imageList =  ?, isSynced =  ? , isSelected =  ?  WHERE id =  ?`;

        const params = [product.assignedProject,
        product.name, product.supplierId, product.supplierName, product.category,
        product.description, product.rating, product.price, product.currency,
        product.moq, product.privateNotes, product.imageFolder,
        product.imageList, product.isSynced, product.isSelected, product.id];

        return this.database.execSQL(prepareQuery, params)
            .then(() => {
                const index = this.products.findIndex((s) => s.id === product.id);
                if (index > -1) {
                    this.products[index] = product;
                    this.products$.next(this.createGroup());
                }
            })
            .catch((error) => console.log(`Product update failed with error`, error));
    }

    delete(product) {
        const prepareQuery = `DELETE FROM product WHERE id = ?`;
        const params = [product.id];

        return this.database.execSQL(prepareQuery, params)
            .then(() => {
                const index = this.products.findIndex((p) => p.id === product.id);
                if (index > -1) {
                    this.products.splice(index, 1);
                    this.products$.next(this.createGroup());
                }
            })
            .catch((error) => console.log(`Product delete failed with error`, error));
    }

    fetchById(id) {
        const prepareQuery = `select * from product where id = ${id}`;

        return this.database.get(prepareQuery);
    }

    fetchDebug() {
        const prepareQuery = `select * from product`;
        this.database.all(prepareQuery, (err, row) => console.log("Row of data was: ", row));
    }

    createTable() {
        const prepareQuery = `CREATE TABLE IF NOT EXISTS product
                                (id INTEGER PRIMARY KEY AUTOINCREMENT,
                                assignedProject TEXT,name TEXT,
                                supplierId INTEGER,
                                supplierName TEXT,
                                category TEXT,
                                description TEXT,
                                rating TEXT,
                                price TEXT,
                                currency TEXT,
                                moq TEXT,
                                privateNotes TEXT,
                                imageFolder TEXT ,
                                imageList TEXT,isSynced TEXT ,
                                isSelected TEXT ,
                                serverId TEXT)`;

        return (new sqlite("sourcie.db"))
            .then((db) => this.database = db)
            .catch((error) => console.log(`Error opening DB: ${error}`))
            .then(() => this.database.execSQL(prepareQuery))
            .then(() => console.log(`Product table is created`))
            .then(() => this.fetchAll())
            .then(() => this.products$.next(this.createGroup()))
            .catch((error) => console.log(`Error creating product table:${error}`));
    }

    private fetchAll() {
        const prepareQuery = `SELECT product.id,product.assignedProject,product.name,
                               supplier.name,product.category,product.description,
                               product.rating,product.price,product.currency,
                               product.moq,product.privateNotes,product.imageFolder,
                               product.imageList,product.isSynced,product.isSelected,
                               product.serverId,product.supplierId,product.supplierName
                               FROM product LEFT JOIN supplier ON supplier.id=product.supplierId`;

        return this.database.all(prepareQuery)
            .then((rows) => this.parseProducts(rows))
            .catch((error) => console.log(`Product query failed with error`, error));
    }

    private parseProducts(rows): any {
        const products = rows.map((row) => {
            const supplierName = row[3] ? row[3] : row[17];
            const imageList = row[12] && row[12].toString().indexOf(",") ? row[12].split(",") : [];
            const product = {
                id: row[0],
                assignedProject: row[1],
                name: row[2],
                supplierName,
                category: row[4],
                description: row[5],
                rating: row[6],
                price: row[7],
                currency: row[8],
                moq: row[9],
                privateNotes: row[10],
                imageFolder: row[11],
                imageList,
                isSynced: row[13],
                isSelected: <boolean>row[14],
                serverId: row[15],
                supplierId: row[16],
                type: "item"
            };

            return product;
        });

        this.products = cloneDeep(products);
    }

    private createGroup(): any {
        this.products = sortBy(this.products, ["assignedProject"]);
        let previousProduct = { assignedProject: "" };
        const groupList = [];
        this.products.map((product) => {
            product.type = "item";
            if (product.assignedProject !== previousProduct.assignedProject) {
                const group = {
                    name: product.assignedProject,
                    type: "group"
                };
                previousProduct = product;
                groupList.push(group);
            }
            groupList.push(product);
        });

        return groupList;
    }

}
