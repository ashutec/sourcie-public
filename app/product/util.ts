export const currencyList = [
    { country: "USD - US Dollar", currency: String.fromCharCode(0xf155) },
    { country: "EUR - Euro", currency: String.fromCharCode(0xf153) },
    { country: "HKD - Hongkong Dollar", currency: String.fromCharCode(0xf155) }
];
