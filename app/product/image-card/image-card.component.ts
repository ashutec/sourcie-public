import { Component, EventEmitter, Input, Output } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

@Component({
    selector: "ns-image-card",
    moduleId: module.id,
    templateUrl: "./image-card.component.html",
    styleUrls: ["image-card.component.css"]
})
export class ImageCardComponent  {
    @Input()  data;
    @Input()  hight;
    @Input()  width;
    @Output() action: EventEmitter<any> = new EventEmitter<any>();

    tapOnImage(data) {
        this.action.emit(data);
    }
}
