import { ModuleWithProviders, NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { CategoryComponent } from "./category/category.component";
import { NewProductComponent } from "./new-product/new-product.component";
import { ProductsComponent } from "./products.component";

const routes: Routes = [
    // { path: "", redirectTo: "/product", pathMatch: "full" },
    { path: "product", component: ProductsComponent },
    { path: "newProduct", component: NewProductComponent },
    { path: "category", component: CategoryComponent }
];

export const productRoutingModule: ModuleWithProviders = RouterModule.forChild(routes);
export const productNavigatableComponents = [
    ProductsComponent,
    NewProductComponent,
    CategoryComponent
];
