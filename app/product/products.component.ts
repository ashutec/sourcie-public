import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import * as cloneDeep from "lodash/cloneDeep";
import * as isEmpty from "lodash/isEmpty";
import { RouterExtensions } from "nativescript-angular/router";
import { LoadingIndicator } from "nativescript-loading-indicator";
import { ListViewEventData, RadListView } from "nativescript-ui-listview";
import "rxjs/add/operator/toPromise";

// import { SourcieScanner } from "../../plugin/nativescript-sourciescanner";
import { SourcieScanner } from "nativescript-sourciescanner";
import { Product } from "../product/product";
import { ProductService } from "../product/product.service";
import { createLoadingOption, onTouchFloatBtnUtility , transition } from "../shared";

import { ProductServerService } from "./product-server.service";
import { currencyList } from "./util";

@Component({
    selector: "ns-products",
    moduleId: module.id,
    templateUrl: "./products.component.html",
    styleUrls: ["products.component.css"]
})
export class ProductsComponent implements OnInit {

    @Output() registerHandlers: EventEmitter<any> = new EventEmitter<any>();
    @Output() selectedItemCount: EventEmitter<any> = new EventEmitter<any>();

    selectedProductsCount;
    imageList: Array<any>;
    products: Array<any>;
    loader = new LoadingIndicator();

    constructor(
        private routerExtensions: RouterExtensions,
        private productService: ProductService,
        private productServerService: ProductServerService) {
        // initialize product table
        this.productService.createTable();
        this.selectedProductsCount = 0;
    }

    ngOnInit(): void {
        this.productService.products$
            .subscribe((products) => this.products = products);
        this.imageList = [];
        for (let index = 0; index < 10; index++) {
            const data = { index, image: "res://imgphoto" };
            this.imageList.push(data);
        }

        // There are handlers to handle toolbar events on home
        this.registerHandlers.emit({
            remove: () => this.deleteProduct(),
            back: () => this.removeSelection()
        });
    }

    templateSelector(item: any, index: number, items: any) {
        return item.type;
    }

    onItemSelected(product) {
        if (this.selectedProductsCount === 0) {
            this.productService.setProduct(cloneDeep(product));
            this.routerExtensions
                .navigate(["/newProduct", { source: "PRODUCTS_LIST" }], { transition });
        } else {
            this.onLongPressItemSelected(product);
        }
    }

    onLongPressItemSelected(product) {
        this.products.map((data) => {
            if (data.id === product.id) {
                data.isSelected = !data.isSelected || data.isSelected === "false" ? "true" : "false";
            }
        });
        this.updateSelectedProductCount();
    }

    onNewProduct() {
        this.productService.setSupplier({});
        this.productService.setProduct({});
        this.routerExtensions.navigate(["/newProduct", { source: "PRODUCT_NEW" }], { transition });
    }

    onTouchFloatBtn(args) {
        onTouchFloatBtnUtility(args);
      }

    deleteProduct() {
        const promiseArray: Array<Promise<any>> = [];
        const options = createLoadingOption(`Deleting products`);
        this.loader.show(options);
        this.products
            .filter((product) => product.isSelected === "true")
            .map((product) => {
                promiseArray.push(
                    this.productServerService.delete(product)
                        .toPromise()
                        .then(() => this.productService.delete(product))
                        .catch((error) => {
                            console.log(`Error in deleting product on server, deleting on local`);

                            return this.productService.delete(product);
                        })
                );
            });
        Promise.all(promiseArray)
            .then(() => {
                this.updateSelectedProductCount();
                this.loader.hide();
            })
            .catch((errors) => {
                console.log(`Error deleting products locally`);
                this.updateSelectedProductCount();
                this.loader.hide();
            });
    }

    removeSelection(): any {
        this.products.map((product) => product.isSelected = "false");
        this.updateSelectedProductCount();
    }

    startQRScanner() {
        const scanner = new SourcieScanner();
        const options = createLoadingOption(`QR code scanner is in progress`);
        this.loader.show(options);
        scanner.startScanner().subscribe((products) => {
            this.saveProductFromScanner(JSON.parse(products.toString()));
        }, (error) => {
            this.loader.hide();
            console.log("Error with QR scanner");
        });
    }

    private updateSelectedProductCount() {
        this.selectedProductsCount = this.products.filter((p) => p.isSelected === "true").length;
        this.selectedItemCount.emit({ count: this.selectedProductsCount });
    }

    private saveProductFromScanner(scannedProducts) {
        const promiseArray: Array<Promise<any>> = [];
        scannedProducts.map((scannedProduct) => {
            const productIndex = this.products
            .findIndex((p) => p.serverId && p.serverId.toString() === scannedProduct.id.toString());
            const filterCurrencyList = currencyList.filter((c) => {
                c.country
                .toUpperCase()
                .includes(scannedProduct.currency.toUpperCase());
            });
            const currency =  filterCurrencyList.length > 0 ? filterCurrencyList[0].country : currencyList[0].country;

            const product: any = {};
            product.name = scannedProduct.name;
            product.assignedProject = "unassigned";
            product.supplierName = scannedProduct.supplier;
            product.category = scannedProduct.category_text;
            product.description = scannedProduct.description;
            product.price = scannedProduct.price;
            product.currency = currency;
            product.moq = scannedProduct.moq;
            product.isSynced = "true";
            product.serverId = scannedProduct.id;
            product.privateNotes = scannedProduct.privateNotes !== null ? scannedProduct.privateNotes : undefined;
            product.imageList = [scannedProduct.image_url1, scannedProduct.image_url2,
            scannedProduct.image_url3, scannedProduct.image_url4, scannedProduct.image_url5]
                .filter((url) => !isEmpty(url));
            if (productIndex > -1) {
                product.supplierId = this.products[productIndex].supplierId;
                promiseArray.push(this.productService.update({ ...this.products[productIndex], ...product }));
            } else {
                promiseArray.push(this.productService.insert(product));
            }
        });

        Promise.all(promiseArray)
        .then(() => this.loader.hide())
        .catch((error) => {
            this.loader.hide();
            console.log(`Error in saving scanned products`);
        });
    }
}
