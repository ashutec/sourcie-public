import {
    AfterViewInit, Component, ElementRef, NgZone,
    OnChanges, OnDestroy, OnInit, QueryList, ViewChildren, ViewContainerRef
} from "@angular/core";
import { EventData } from "data/observable";
import { ModalDialogOptions, ModalDialogService } from "nativescript-angular/modal-dialog";
import { RouterExtensions } from "nativescript-angular/router";
import { Task } from "nativescript-background-http";
import { requestPermissions, takePicture } from "nativescript-camera";
import { LoadingIndicator } from "nativescript-loading-indicator";
import * as connectivity from "tns-core-modules/connectivity";

import { Button } from "ui/button";
import { Image } from "ui/image";
import { GridLayout, GridUnitType, ItemSpec } from "ui/layouts/grid-layout";
import { Page } from "ui/page";
import { Progress } from "ui/progress";
import { PercentLength } from "ui/styling/style-properties";
import { TextField } from "ui/text-field";
import { TextView } from "ui/text-view";

import * as fs from "file-system";
import { fromAsset, fromFile, ImageSource } from "image-source";
import * as isEmpty from "lodash/isEmpty";

import {
    createLoadingOption, ListPickerComponent,
    onTouchFloatBtnUtility, renderCarouselSlides, transition
} from "../../shared";

import { Product } from "../product";
import { ProductServerService } from "../product-server.service";
import { ProductService } from "../product.service";
import { currencyList } from "../util";

@Component({
    selector: "ns-newProduct",
    moduleId: module.id,
    templateUrl: "new-product.component.html",
    styleUrls: ["new-product.component.css"]
})

export class NewProductComponent implements OnInit, OnDestroy {

    @ViewChildren("textField") textFields: QueryList<ElementRef>;
    @ViewChildren("textView") textViews: QueryList<ElementRef>;

    product: Product = new Product();
    loader = new LoadingIndicator();
    task: Task;
    isProgress: boolean = false;
    selectedCurrency;
    notesShow: boolean = false;
    ratings = ["OPP", "MPP", "HPP"];
    carousel;
    supplier;
    // This is magic variable
    // it delay complex UI show Router navigation can finish first to have smooth transition
    renderView = false;

    constructor(
        private modalDialogService: ModalDialogService,
        private viewContainerRef: ViewContainerRef,
        private routerExtensions: RouterExtensions,
        private productService: ProductService,
        private ngZone: NgZone,
        private productServerService: ProductServerService,
        private page: Page
    ) { }

    ngOnInit() {

        this.productService.productChanged$.subscribe((product) => {
            this.product = product;
            this.setProductData();
        });
        // update to variable needed to do in ngZone otherwise it did not understand it
        this.page.on("loaded", () => this.ngZone.run(() => this.renderView = true));
    }

    ngOnDestroy() {
        this.page.off("loaded");
    }

    setProductData() {
        this.product.assignedProject = this.product.assignedProject ? this.product.assignedProject : "unassigned";
        if (this.product.rating === undefined) {
            this.product.rating = this.ratings[0];
        }
        if (!this.product.currency) {
            this.product.currency = currencyList[0].country;
            this.selectedCurrency = currencyList[0].currency;
        } else {
            currencyList.map((res) => {
                if (res.country === this.product.currency) {
                    this.selectedCurrency = res.currency;
                }
            });
        }

        if (!this.product.imageList) {
            this.product.imageList = [];
        }

        this.notesShow = isEmpty(this.product.privateNotes) ? false : true;
    }

    onCarouselLoad(args?: EventData): void {
        try {
            // This is trick to call function when new image is save
            // This is not healthy way to do it and may slow down app

            this.carousel = args && args.object ? args.object : this.carousel;
            const images = this.product.imageList && this.product.imageList.length > 0 ?
                this.product.imageList : ["res://imgphoto"];
            // This is trick to avoid having exception when there is no image first time
            if (images.length) {
                this.carousel.removeChildren();
            }

            renderCarouselSlides(
                this.carousel,
                images,
                (imageData) => {
                    const layout = new GridLayout();
                    layout.addRow(new ItemSpec(1, GridUnitType.AUTO));
                    layout.addColumn(new ItemSpec(1, GridUnitType.STAR));
                    const image = new Image();
                    image.width = PercentLength.parse("100%");
                    image.height = PercentLength.parse("100%");
                    image.alignSelf = "center";
                    image.src = imageData;
                    image.className = "image";
                    image.stretch = "aspectFill";
                    image.loadMode = "async";
                    GridLayout.setRow(image, 0);
                    GridLayout.setRowSpan(image, 1);
                    GridLayout.setColumn(image, 0);
                    GridLayout.setColumnSpan(image, 0);
                    layout.addChild(image);

                    return layout;
                }
            );

        } catch (error) {
            console.log("Error on onCarouselLoad" + error);
        }

    }
    onSelectCountry() {
        const countryName = [];
        currencyList.forEach((res) => { countryName.push(res.country); });
        const options: ModalDialogOptions = {
            viewContainerRef: this.viewContainerRef,
            fullscreen: false,
            context: { list: countryName }
        };
        this.modalDialogService.showModal(ListPickerComponent, options).then((result) => {
            if (result !== undefined) {
                this.selectedCurrency = currencyList[result].currency;
                this.product.currency = currencyList[result].country;
            }
        });
    }
    onSelectRating() {
        const options: ModalDialogOptions = {
            viewContainerRef: this.viewContainerRef,
            fullscreen: false,
            context: { list: this.ratings }
        };
        this.modalDialogService.showModal(ListPickerComponent, options).then((result) => {
            if (result) {
                this.product.rating = this.ratings[result];
            }
        });
    }

    onBlurNotes() {
        if (this.product.privateNotes === null ||
            this.product.privateNotes === "" ||
            this.product.privateNotes === undefined) {
            setTimeout(() => this.notesShow = false, 2);
        }
    }

    onFocusNotes() {
        this.notesShow = true;
    }

    gotoSupplierList() {
        this.routerExtensions.navigate(["/selectSupplier"], { transition });
    }

    gotoCategoryList() {
        this.routerExtensions.navigate(["/category"], { transition });
    }
    goBack() {
        this.routerExtensions.back();
    }

    onSave() {
        const connectionType = connectivity.getConnectionType();
        switch (connectionType) {
            case connectivity.connectionType.none:
                (this.product.id ? this.updateProductOnLocal() : this.saveProductOnLocal({}));
                break;
            case connectivity.connectionType.wifi:
            case connectivity.connectionType.mobile:
                (this.product.id ? this.updateProductOnLocal() : this.saveProductOnServer());
                break;
        }
    }

    onTakePictureTap() {
        requestPermissions();
        takePicture({ width: 300, height: 300, keepAspectRatio: false, saveToGallery: false })
            .then((imageAsset) => {
                setTimeout(() => this.saveImage(imageAsset), 100);
            })
            .catch((error) => console.log(`camera error`, error));
    }

    saveImage(imageAsset) {
        const source = new ImageSource();
        source.fromAsset(imageAsset).then((imageSource) => {
            const folder = fs.knownFolders.documents();
            const fileName = `${Math.random().toString(36).substr(2, 10)}.png`;
            const path = fs.path.join(folder.path, fileName);
            const saved = imageSource.saveToFile(path, "png");
            if (saved) {
                if (!this.product.imageList) {
                    this.product.imageList = [];
                }
                this.product.imageList.unshift(path);
                this.onCarouselLoad();
            }
        });
    }

    saveProductOnServer() {
        if (this.product.imageList[0]) {
            this.isProgress = true;
            const progress: Progress = <Progress>this.page.getViewById("prgressmultiid");
            progress.value = 0;
            const options = createLoadingOption(`Product is uploading`);
            this.loader.show(options);
            this.task = this.productServerService.save(this.product);
            this.task.on("progress", (e) => {

                progress.value = e.currentBytes;
                progress.maxValue = e.totalBytes;

            });
            this.task.on("error", (e) => this.logEvent(e));
            this.task.on("responded", (e) => this.logEvent(e));
            this.task.on("complete", (e) => this.logEvent(e));
        } else {
            alert("Please save at least one image ");
            this.loader.hide();
        }
    }

    saveProductOnLocal(data) {
        if (data && data.product && data.product.id) {
            this.product.serverId = data.product.id;
        }
        this.product.isSynced = "true";
        this.productService.insert(this.product)
            .then((id) => this.redirectToPreviousPage())
            .catch((error) => alert(`there was problem saving product`));
    }

    updateProductOnLocal(data?) {
        if (this.product.privateNotes === null) {
            this.product.privateNotes = "";
        }
        this.productService.update(this.product)
            .then((id) => this.redirectToPreviousPage())
            .catch((error) => alert(`there was problem saving product`));
    }

    logEvent(e) {
        switch (e.eventName) {
            case "error":
                alert("Upload error" + e);
                this.loader.hide();
                break;
            case "responded":
                const data = JSON.parse(e.data);
                /** save on local call Here */
                if (this.product.isSynced !== "true") {
                    this.saveProductOnLocal(data);
                }
            default:
                break;
        }
    }

    redirectToPreviousPage() {
        this.ngZone.run(() => {
            this.isProgress = false;
            this.loader.hide();
            this.routerExtensions.back();
        });
    }

    onTouchFloatBtn(args) {
        onTouchFloatBtnUtility(args);
    }

    hideKeyboard() {
        if (this.textFields.length && this.textViews.length) {
            this.textFields
                .toArray()
                .concat(this.textViews.toArray())
                .map((el) => el.nativeElement.dismissSoftInput());

        }
    }

    onTapFocus(fieldId) {
        if (this.textFields.length && this.textViews.length) {
            this.textFields
                .toArray()
                .concat(this.textViews.toArray())
                .filter((t) => t.nativeElement.id === fieldId)
                .map((el, index) => index === 0 ? el.nativeElement.focus() : "");
        }
    }
}
