import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { session, Session } from "nativescript-background-http";

import { BASE_API_URL } from "../shared";

@Injectable()
export class ProductServerService {
    private session: Session;
    constructor(private http: HttpClient) {
        this.session = session("image-upload");
    }

    save(product) {
        const params = [
            { name: "supplier", value: product.supplierName },
            { name: "name", value: product.name },
            { name: "category", value: product.category },
            { name: "moq", value: product.moq },
            { name: "description", value: product.description },
            // { name: "rating", value: product.rating },
            { name: "price", value: product.price },
            { name: "currency", value: product.currency },
            { name: "image_1", filename: product.imageList[0], mimeType: "image/png" },
            { name: "image_2", filename: product.imageList[1], mimeType: "image/png" },
            { name: "image_3", filename: product.imageList[2], mimeType: "image/png" },
            { name: "image_4", filename: product.imageList[3], mimeType: "image/png" },
            { name: "image_5", filename: product.imageList[4], mimeType: "image/png" }

        ];
        const url = `${BASE_API_URL}/products.json`;
        const request = {
            url,
            method: "POST"
            };

        return this.session.multipartUpload(params, request);

    }

    delete(product) {
        const url = `${BASE_API_URL}/products/${product.serverId}.json`;

        return this.http.delete(url);
    }
}
