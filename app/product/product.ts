export class Product {
    id: string;
    assignedProject: string;
    name: string;
    supplierName: string;
    category: string;
    description: string;
    rating: string;
    price: number;
    currency: string;
    moq: number;
    privateNotes: string;
    imageFolder: string;
    imageList: Array<string>;
    isSynced: string;
    isSelected: string;
    serverId?: string;
    supplierId?: string;
    type?: string;
}
