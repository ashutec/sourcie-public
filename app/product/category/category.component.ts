import { Component, OnDestroy, OnInit, ViewContainerRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";

import { Page } from "tns-core-modules/ui/page/page";
import { Product } from "../product";
import { ProductService } from "../product.service";

@Component({
    selector: "tns-product-category",
    moduleId: module.id,
    templateUrl: "category.component.html",
    styleUrls: ["category.component.css"]
})

export class CategoryComponent implements OnDestroy, OnInit {
    list;
    renderView = false;

    constructor(private routerExtensions: RouterExtensions,
                private productService: ProductService,
                private page: Page) { }

    ngOnInit() {
        this.list = [
            {
                name: "A", type: "group"
            },
            {
                name: "Adhesive Tapes", type: "item"
            },
            {
                name: "C", type: "group"
            },
            {
                name: "Car Accessories", type: "item"
            },
            {
                name: "Construction Equipment", type: "item"
            },
            {
                name: "F", type: "group"
            },
            {
                name: "Fastening", type: "item"
            },
            {
                name: "Furniture and Home Decoration", type: "item"
            },
            {
                name: "G", type: "group"
            },
            {
                name: "Generators & Welding", type: "item"
            },
            {
                name: "Garden Machinery", type: "item"
            },
            {
                name: "Garden & Outdoor", type: "item"
            },
            {
                name: "H", type: "group"
            },
            {
                name: "High Pressure & Vacuum Cleaners", type: "item"
            },
            {
                name: "Hand Tools", type: "item"
            },
            {
                name: "Hardware", type: "item"
            },
            {
                name: "HEPAC", type: "item"
            },
            {
                name: "L", type: "group"
            },
            {
                name: "Lighting & Electric Equipment", type: "item"
            },

            {
                name: "P", type: "group"
            },
            {
                name: "Power Tools", type: "item"
            },
            {
                name: "Pneumatic Tools & Compressors", type: "item"
            },
            {
                name: "Painting Tools & Accessories", type: "item"
            },

            {
                name: "S", type: "group"
            },
            {
                name: "Smart Home,Safety & Security", type: "item"
            },

            {
                name: "W", type: "group"
            },
            {
                name: "Work Safety", type: "item"
            },
            {
                name: "Workshop & Storage", type: "item"
            }
        ];

        this.page.on("loaded", () => this.renderView = true);
    }

    ngOnDestroy() {
        this.page.off("loaded");
    }

    onSelect(category) {
        this.productService.setCategory(category);
        this.routerExtensions.back();
    }

    goBack() {
        this.routerExtensions.back();
    }
    templateSelector(item: any, index: number, items: any) {
        return item.type;
    }
}
