import { NgModule , NO_ERRORS_SCHEMA } from "@angular/core";
import { homeNavigatableComponents, homeRoutingModule } from "./home.routing";

// Declare Module Here
import { ProductModule } from "../product";
import { SharedModule } from "../shared";
import { SupplierModule } from "../supplier";

@NgModule({
    imports: [
        SharedModule,
        homeRoutingModule,
        ProductModule,
        SupplierModule
    ],
    declarations: [
        homeNavigatableComponents
    ],
    providers: [],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class HomeModule { }
