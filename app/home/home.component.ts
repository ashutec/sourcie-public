import { Component, NgZone, OnDestroy, OnInit } from "@angular/core";
import { Page } from "ui/page";
import { TabView } from "ui/tab-view";

@Component({
    selector: "ns-home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ["home.component.css"]
})
export class HomeComponent implements OnDestroy, OnInit {
    selectedItemCount;
    removeItemHandlers: any;
    tabSelectedIndex;
    // This is magic variable
    // it delay complex UI show Router navigation can finish first to have smooth transition
    renderView = false;

    constructor(private page: Page, private ngZone: NgZone) {
        this.selectedItemCount = 0;
        this.tabSelectedIndex = 1;
        this.removeItemHandlers = {};
      }

    ngOnInit() {
        this.page.actionBarHidden = true;
        this.page.on("loaded", () => this.ngZone.run(() => this.renderView = true));
    }

    ngOnDestroy() {
        this.page.off("loaded");
    }

    setHandlers(event, tabIndex) {
        this.removeItemHandlers[tabIndex] = event;
    }

    setSelectedItemCount(event) {
        this.selectedItemCount = event.count ? event.count : 0;
    }

    deleteOnTap(args) {
        if (this.removeItemHandlers[this.tabSelectedIndex] &&
            this.removeItemHandlers[this.tabSelectedIndex].remove) {
            this.removeItemHandlers[this.tabSelectedIndex].remove();
        }
    }

    backOnTap(args) {
        if (this.removeItemHandlers[this.tabSelectedIndex] &&
            this.removeItemHandlers[this.tabSelectedIndex].back) {
            this.removeItemHandlers[this.tabSelectedIndex].back();
        }
    }
}
